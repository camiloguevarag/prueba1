﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Prueba1Git.Web.Startup))]
namespace Prueba1Git.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
